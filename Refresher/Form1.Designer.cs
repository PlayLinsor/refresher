﻿namespace Refresher
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.TrayIconsList = new System.Windows.Forms.ImageList(this.components);
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextTrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TimeShowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editSite = new System.Windows.Forms.TextBox();
            this.Label_1 = new System.Windows.Forms.Label();
            this.label_2 = new System.Windows.Forms.Label();
            this.editDelay = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.SleepTimer = new System.Windows.Forms.Timer(this.components);
            this.HelpHiht = new System.Windows.Forms.ToolTip(this.components);
            this.contextTrayMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // Timer
            // 
            this.Timer.Interval = 400000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // TrayIconsList
            // 
            this.TrayIconsList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TrayIconsList.ImageStream")));
            this.TrayIconsList.TransparentColor = System.Drawing.Color.Transparent;
            this.TrayIconsList.Images.SetKeyName(0, "Icon_1.ico");
            this.TrayIconsList.Images.SetKeyName(1, "Icon_2.ico");
            this.TrayIconsList.Images.SetKeyName(2, "Icon_3.ico");
            this.TrayIconsList.Images.SetKeyName(3, "Icon_4.ico");
            // 
            // TrayIcon
            // 
            this.TrayIcon.ContextMenuStrip = this.contextTrayMenu;
            this.TrayIcon.Text = "TrayIcon";
            this.TrayIcon.Visible = true;
            this.TrayIcon.DoubleClick += new System.EventHandler(this.TrayIcon_DoubleClick);
            // 
            // contextTrayMenu
            // 
            this.contextTrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TimeShowMenuItem,
            this.toolStripMenuItem1,
            this.обновитьToolStripMenuItem,
            this.настройкиToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.contextTrayMenu.Name = "contextTrayMenu";
            this.contextTrayMenu.Size = new System.Drawing.Size(173, 98);
            // 
            // TimeShowMenuItem
            // 
            this.TimeShowMenuItem.Name = "TimeShowMenuItem";
            this.TimeShowMenuItem.Size = new System.Drawing.Size(172, 22);
            this.TimeShowMenuItem.Text = "Обновляю...";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(169, 6);
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.обновитьToolStripMenuItem.Text = "Обновить сейчас";
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.обновитьToolStripMenuItem_Click);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.настройкиToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // editSite
            // 
            this.editSite.Location = new System.Drawing.Point(49, 9);
            this.editSite.Name = "editSite";
            this.editSite.Size = new System.Drawing.Size(199, 20);
            this.editSite.TabIndex = 2;
            // 
            // Label_1
            // 
            this.Label_1.AutoSize = true;
            this.Label_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label_1.Location = new System.Drawing.Point(3, 9);
            this.Label_1.Name = "Label_1";
            this.Label_1.Size = new System.Drawing.Size(40, 16);
            this.Label_1.TabIndex = 3;
            this.Label_1.Text = "Сайт";
            // 
            // label_2
            // 
            this.label_2.AutoSize = true;
            this.label_2.Location = new System.Drawing.Point(254, 11);
            this.label_2.Name = "label_2";
            this.label_2.Size = new System.Drawing.Size(72, 13);
            this.label_2.TabIndex = 4;
            this.label_2.Text = "Период (сек)";
            // 
            // editDelay
            // 
            this.editDelay.Location = new System.Drawing.Point(332, 9);
            this.editDelay.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.editDelay.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.editDelay.Name = "editDelay";
            this.editDelay.Size = new System.Drawing.Size(43, 20);
            this.editDelay.TabIndex = 5;
            this.editDelay.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(381, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 21);
            this.button1.TabIndex = 6;
            this.button1.Text = "сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SleepTimer
            // 
            this.SleepTimer.Interval = 5000;
            this.SleepTimer.Tick += new System.EventHandler(this.SleepTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 38);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.editDelay);
            this.Controls.Add(this.label_2);
            this.Controls.Add(this.Label_1);
            this.Controls.Add(this.editSite);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авто обновлялка связи v 0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.contextTrayMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.editDelay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.ImageList TrayIconsList;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.ContextMenuStrip contextTrayMenu;
        private System.Windows.Forms.ToolStripMenuItem TimeShowMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.TextBox editSite;
        private System.Windows.Forms.Label Label_1;
        private System.Windows.Forms.Label label_2;
        private System.Windows.Forms.NumericUpDown editDelay;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.Timer SleepTimer;
        private System.Windows.Forms.ToolTip HelpHiht;
    }
}

